/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef SENSORS_H    /* Guard against multiple inclusion */
#define SENSORS_H

// *****************************************************************************

#include "../mcc_generated_files/mcc.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************


void SensorsEnable(void);
void SensorsDisable(void);

bool IsRearSensorInterrupted(uint8_t feederLane) ;
bool IsFrontSensorInterrupted(uint8_t feederLane);


// *****************************************************************************

#endif /* SENSORS_H */

/* *****************************************************************************
 End of File
 */
