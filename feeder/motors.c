// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "motors.h"

// *****************************************************************************

//defines
//structs
//varibles
//constants

// *****************************************************************************

//Local function definitions

// *****************************************************************************

void MotorsEnable(void) {
    MOTOR_SLP_SetHigh();
}

void MotorsDisable(void) {
    MOTOR_SLP_SetLow();
    HaltLaneMotor(1);
    HaltLaneMotor(2);
    HaltLaneMotor(3);
    HaltLaneMotor(4);
}

void DriveLaneMotorForward(uint8_t feederLane) {
    switch (feederLane) {
        case 1:
            MOTOR_F_1_SetHigh();
            break;
        case 2:
            MOTOR_F_2_SetHigh();
            break;
        case 3:
            MOTOR_F_3_SetHigh();
            break;
        case 4:
            MOTOR_F_4_SetHigh();
            break;

    }
}

void DriveLaneMotorReverse(uint8_t feederLane) {
    switch (feederLane) {
        case 1:
            MOTOR_R_1_SetHigh();
            break;
        case 2:
            MOTOR_R_2_SetHigh();
            break;
        case 3:
            MOTOR_R_3_SetHigh();
            break;
        case 4:
            MOTOR_R_4_SetHigh();
            break;
    }
}

void BreakLaneMotor(uint8_t feederLane) {
    switch (feederLane) {
        case 1:
            MOTOR_F_1_SetHigh();
            MOTOR_R_1_SetHigh();
            break;
        case 2:
            MOTOR_F_2_SetHigh();
            MOTOR_R_2_SetHigh();
            break;
        case 3:
            MOTOR_F_3_SetHigh();
            MOTOR_R_3_SetHigh();
            break;
        case 4:
            MOTOR_F_4_SetHigh();
            MOTOR_R_4_SetHigh();
            break;
    }
}

void HaltLaneMotor(uint8_t feederLane) {
    switch (feederLane) {
        case 1:
            MOTOR_F_1_SetLow();
            MOTOR_R_1_SetLow();
            break;
        case 2:
            MOTOR_F_2_SetLow();
            MOTOR_R_2_SetLow();
            break;
        case 3:
            MOTOR_F_3_SetLow();
            MOTOR_R_3_SetLow();
            break;
        case 4:
            MOTOR_F_4_SetLow();
            MOTOR_R_4_SetLow();
            break;
    }
}

void DriveTapeMotorForward(void) {
    MOTOR_F_T_SetHigh();
}

void DriveTapeMotorReverse(void) {
    MOTOR_R_T_SetHigh();
}

void HaltTapeMotor(void) {
    MOTOR_R_T_SetLow();
    MOTOR_F_T_SetLow();
}


/* *****************************************************************************
 End of File
 */
