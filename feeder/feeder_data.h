// *****************************************************************************
/*
  @File Name
    feeder_data.c
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include <stdbool.h>
#include <stdint.h>

// *****************************************************************************

typedef struct {
    bool laneEnabled;
    bool halfPitchPosible;
    bool pwmControlled;
    uint8_t feedPitch;
    uint8_t advancePitch;
    uint8_t retractPitch;
    uint8_t pwm;
    uint8_t exceptionTime;
} fdr_lane;

fdr_lane lane [5];

// *****************************************************************************

//Feeder global settings

//#define FEEDER_TESTING_ON

// *****************************************************************************

//Feeder ID settings selection 

//#define     FEEDER_ID_F_10              //4x8 plastic
//#define     FEEDER_ID_F_11              //4x8 plastic    
//#define     FEEDER_ID_F_12              //4x8 plastic    
//#define     FEEDER_ID_F_13              //3x12
//#define     FEEDER_ID_F_14              //1x16, 1x12, 0x8


//#define     FEEDER_ID_F_17              //4x8 paper 
#define     FEEDER_ID_F_18              //4x8 paper 

// *****************************************************************************

#ifdef FEEDER_ID_F_10
#define     FEEDER_ID_NUMBER            10

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           1
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           1
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           1
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           1
#define     LANE_3_ADVANCE_PITCH        1
#define     LANE_3_RETRACT_PITCH        1
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           1
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           1
#define     LANE_4_ADVANCE_PITCH        1
#define     LANE_4_RETRACT_PITCH        1
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 10

#ifdef FEEDER_ID_F_11
#define     FEEDER_ID_NUMBER            11

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           1
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           1
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           1
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           1
#define     LANE_3_ADVANCE_PITCH        1
#define     LANE_3_RETRACT_PITCH        1
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           1
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           1
#define     LANE_4_ADVANCE_PITCH        1
#define     LANE_4_RETRACT_PITCH        1
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 11

#ifdef FEEDER_ID_F_12
#define     FEEDER_ID_NUMBER            12

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           1
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           1
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           1
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           1
#define     LANE_3_ADVANCE_PITCH        1
#define     LANE_3_RETRACT_PITCH        1
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           1
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           1
#define     LANE_4_ADVANCE_PITCH        1
#define     LANE_4_RETRACT_PITCH        1
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 12

#ifdef FEEDER_ID_F_13
#define     FEEDER_ID_NUMBER            13

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           2
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           2
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           1
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           2
#define     LANE_3_ADVANCE_PITCH        1
#define     LANE_3_RETRACT_PITCH        1
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           0
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           0
#define     LANE_4_ADVANCE_PITCH        0
#define     LANE_4_RETRACT_PITCH        0
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 13

#ifdef FEEDER_ID_F_14
#define     FEEDER_ID_NUMBER            14

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           3
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           2
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           0
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           0
#define     LANE_3_ADVANCE_PITCH        0
#define     LANE_3_RETRACT_PITCH        0
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           0
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           0
#define     LANE_4_ADVANCE_PITCH        0
#define     LANE_4_RETRACT_PITCH        0
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 14

#ifdef FEEDER_ID_F_17
#define     FEEDER_ID_NUMBER            17

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           1
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           1
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           1
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           1
#define     LANE_3_ADVANCE_PITCH        1
#define     LANE_3_RETRACT_PITCH        1
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           1
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           1
#define     LANE_4_ADVANCE_PITCH        1
#define     LANE_4_RETRACT_PITCH        1
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 17

#ifdef FEEDER_ID_F_18
#define     FEEDER_ID_NUMBER            18

#define     LANE_1_IS_ENABLED           1
#define     LANE_1_HALF_PITCH_POSIBLE   0
#define     LANE_1_PWM_CONTROLLED       0
#define     LANE_1_FEED_PITCH           1
#define     LANE_1_ADVANCE_PITCH        1
#define     LANE_1_RETRACT_PITCH        1
#define     LANE_1_PWM                  0
#define     LANE_1_EXCEPTION_TIME       0

#define     LANE_2_IS_ENABLED           1
#define     LANE_2_HALF_PITCH_POSIBLE   0
#define     LANE_2_PWM_CONTROLLED       0
#define     LANE_2_FEED_PITCH           1
#define     LANE_2_ADVANCE_PITCH        1
#define     LANE_2_RETRACT_PITCH        1
#define     LANE_2_PWM                  0
#define     LANE_2_EXCEPTION_TIME       0

#define     LANE_3_IS_ENABLED           1
#define     LANE_3_HALF_PITCH_POSIBLE   0
#define     LANE_3_PWM_CONTROLLED       0
#define     LANE_3_FEED_PITCH           1
#define     LANE_3_ADVANCE_PITCH        1
#define     LANE_3_RETRACT_PITCH        1
#define     LANE_3_PWM                  0
#define     LANE_3_EXCEPTION_TIME       0

#define     LANE_4_IS_ENABLED           1
#define     LANE_4_HALF_PITCH_POSIBLE   0
#define     LANE_4_PWM_CONTROLLED       0
#define     LANE_4_FEED_PITCH           1
#define     LANE_4_ADVANCE_PITCH        1
#define     LANE_4_RETRACT_PITCH        1
#define     LANE_4_PWM                  0
#define     LANE_4_EXCEPTION_TIME       0
#endif      //FDR 18

// *****************************************************************************

//Local function definitions

// *****************************************************************************


void FeederDataInit(void);


/* *****************************************************************************
 End of File
 */
