// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "parser.h"
#include "../mcc_generated_files/mcc.h"
#include "commands.h"
#include "feeder_data.h"

// *****************************************************************************

#define NUMBER_OF_COMMANDS          13
uint8_t bufic;

// *****************************************************************************

void ReceiveMessage(void);
void TransmitMessages(void);
bool ReadyForParsing(void);
void ParseMessages(void);

// *****************************************************************************

void CommunicationHandler(void) {

    ReceiveMessage();
    TransmitMessages();
}

void ReceiveMessage(void) {

    //Read all messages in receive buffer
    if (EUSART1_is_rx_ready()) {
        bufic = EUSART1_Read();
        //EUSART1_Write(bufic); //Echo?
    } else {
        //bufic = 0;
        return;
    }

    //Check for end character
    //Check CRC

    //Parse
    //if (ReadyForParsing()) {
    ParseMessages();
    //    }

}

void TransmitMessages(void) {
    //
    //    if (EUSART1_is_tx_ready) {
    //        EUSART1_Write('s');
    //    }
}

bool ReadyForParsing(void);

void ParseMessages(void) {
    //remove 0x00
    //bufic--;


    //Global commands
    if (bufic == 250) StartTest();
    if (bufic == 251) HaltTest();

    //check if command is intended for this serial number, or if command is global

    if (!((bufic >= ((FEEDER_ID_NUMBER - 1) * NUMBER_OF_COMMANDS))&&(bufic < (FEEDER_ID_NUMBER * NUMBER_OF_COMMANDS)))) {
        return;
    }

    if (bufic < 249) {
        //clean command
        bufic %= NUMBER_OF_COMMANDS;
    }

    switch (bufic) {

            //Advance lane
        case 0:
            AdvanceLane(1);
            break;
        case 1:
            AdvanceLane(2);
            break;
        case 2:
            AdvanceLane(3);
            break;
        case 3:
            AdvanceLane(4);
            break;

            //Feed lane
        case 4:
            FeedLane(1);
            break;
        case 5:
            FeedLane(2);
            break;
        case 6:
            FeedLane(3);
            break;
        case 7:
            FeedLane(4);
            break;

            //Retract lane
        case 8:
            RetractLane(1);
            break;
        case 9:
            RetractLane(2);
            break;
        case 10:
            RetractLane(3);
            break;
        case 11:
            RetractLane(4);
            break;

        default: break;

    }

}

/* *****************************************************************************
 End of File
 */
