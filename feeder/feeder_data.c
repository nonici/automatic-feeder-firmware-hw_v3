// *****************************************************************************
/*
  @File Name
    feeder_data.c
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "feeder_data.h"

// *****************************************************************************

//defines
//structs
//varibles
//constants

// *****************************************************************************

//Local function definitions

// *****************************************************************************

void FeederDataInit(void) {

    lane[0].laneEnabled = 0;
    lane[0].halfPitchPosible = 0;
    lane[0].pwmControlled = 0;
    lane[0].feedPitch = 0;
    lane[0].advancePitch = 0;
    lane[0].retractPitch = 0;
    lane[0].pwm = 0;
    lane[0].exceptionTime = 0;

    lane[1].laneEnabled = LANE_1_IS_ENABLED;
    lane[1].halfPitchPosible = LANE_1_HALF_PITCH_POSIBLE;
    lane[1].pwmControlled = LANE_1_PWM_CONTROLLED;
    lane[1].feedPitch = LANE_1_FEED_PITCH;
    lane[1].advancePitch = LANE_1_ADVANCE_PITCH;
    lane[1].retractPitch = LANE_1_RETRACT_PITCH;
    lane[1].pwm = LANE_1_PWM;
    lane[1].exceptionTime = LANE_1_EXCEPTION_TIME;

    lane[2].laneEnabled = LANE_2_IS_ENABLED;
    lane[2].halfPitchPosible = LANE_2_HALF_PITCH_POSIBLE;
    lane[2].pwmControlled = LANE_2_PWM_CONTROLLED;
    lane[2].feedPitch = LANE_2_FEED_PITCH;
    lane[2].advancePitch = LANE_2_ADVANCE_PITCH;
    lane[2].retractPitch = LANE_2_RETRACT_PITCH;
    lane[2].pwm = LANE_2_PWM;
    lane[2].exceptionTime = LANE_2_EXCEPTION_TIME;

    lane[3].laneEnabled = LANE_3_IS_ENABLED;
    lane[3].halfPitchPosible = LANE_3_HALF_PITCH_POSIBLE;
    lane[3].pwmControlled = LANE_3_PWM_CONTROLLED;
    lane[3].feedPitch = LANE_3_FEED_PITCH;
    lane[3].advancePitch = LANE_3_ADVANCE_PITCH;
    lane[3].retractPitch = LANE_3_RETRACT_PITCH;
    lane[3].pwm = LANE_3_PWM;
    lane[3].exceptionTime = LANE_3_EXCEPTION_TIME;

    lane[4].laneEnabled = LANE_4_IS_ENABLED;
    lane[4].halfPitchPosible = LANE_4_HALF_PITCH_POSIBLE;
    lane[4].pwmControlled = LANE_4_PWM_CONTROLLED;
    lane[4].feedPitch = LANE_4_FEED_PITCH;
    lane[4].advancePitch = LANE_4_ADVANCE_PITCH;
    lane[4].retractPitch = LANE_4_RETRACT_PITCH;
    lane[4].pwm = LANE_4_PWM;
    lane[4].exceptionTime = LANE_4_EXCEPTION_TIME;

}

/* *****************************************************************************
 End of File
 */
