// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "commands.h"
#include "driver.h"
#include "feeder_data.h"

// *****************************************************************************

#ifdef FEEDER_TESTING_ON
volatile bool testingIsOn = 1;
#else
volatile bool testingIsOn = 0;
#endif

// *****************************************************************************

//Local function definitions

// *****************************************************************************

void FeedLane(uint8_t feederLane) {
    if (!lane[feederLane].laneEnabled) {
        //lane doesn't exists
        return;
    }

    AdvanceLaneForPitch(feederLane, lane[feederLane].feedPitch);
}

void AdvanceLane(uint8_t feederLane) {
    if (!lane[feederLane].laneEnabled) {
        //lane doesn't exists
        return;
    }

    AdvanceLaneForPitch(feederLane, lane[feederLane].advancePitch);
}

void RetractLane(uint8_t feederLane) {
    if (!lane[feederLane].laneEnabled) {
        //lane doesn't exists
        return;
    }

    RetractLaneForPitch(feederLane, lane[feederLane].retractPitch);
}

void StartTest(void) {
    testingIsOn = 1;
}

void HaltTest(void) {
    testingIsOn = 0;
}

bool IsTestingOn(void) {
    return (testingIsOn);
}
/* *****************************************************************************
 End of File
 */
