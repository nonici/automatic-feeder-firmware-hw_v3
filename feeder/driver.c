// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "driver.h"
#include "feeder_data.h"

// *****************************************************************************

//defines
//structs
//varibles
//constants

bool exceptionOccured = 0;
bool feedFinished = 0;
bool feedTimeout = 0;
uint8_t exceptionGuardLane = 0;

// *****************************************************************************

void ExceptionGuard(uint32_t feederLane, uint32_t pitch);
void DropGuard(void);

// *****************************************************************************

//Core function

//Global functions

//Local functions

void AdvanceLaneForPitch(uint8_t feederLane, uint8_t feedingPitch) {


    ExceptionGuard(feederLane, feedingPitch);

    //Start Motors
    MotorsEnable();
    DriveLaneMotorForward(feederLane);

    //Loop once for each 4mm of pitch
    while (feedingPitch--) {

        TMR1_Reload();

        //_____|____
        //____-|-___
        //Roll if still interrupted from previous session, exits when no photointerrupts 
        //4mm pitch
        while (IsFrontSensorInterrupted(feederLane)) {
            //2mm pitch
            // while (((IsRearSensorInterrupted(feederLane)) || (IsFrontSensorInterrupted(feederLane)))) {  
            if (exceptionOccured) {
                break;
            }
        }

        //_--__|__________
        //_____|____--____
        //Roll while no photointerrupts, exits when photointerrupt occurs
        //4mm pitch
        while (!IsFrontSensorInterrupted(feederLane)) {
            //2mm pitch
            //while ((!IsRearSensorInterrupted(feederLane) && !IsFrontSensorInterrupted(feederLane))) {     
            if (exceptionOccured) {
                break;
            }
        }

        //____--|__
        //_________
        //Roll until photointerrupt end, exits when no photointerrupts
        //4mm pitch
        while (IsFrontSensorInterrupted(feederLane)) {
            //2mm pitch
            //while ((IsRearSensorInterrupted(feederLane) || IsFrontSensorInterrupted(feederLane))) {       
            if (exceptionOccured) {
                break;
            }

        }
    }

    BreakLaneMotor(feederLane);
    feedFinished = 1;

    DropGuard();

    //Halt Motors
    feedFinished = 1;

    //HaltTapeMotor();

    DropGuard();

    __delay_ms(50);
    MotorsDisable();


}

void RetractLaneForPitch(uint8_t feederLane, uint8_t feedingPitch) {

    ExceptionGuard(feederLane, feedingPitch);

    MotorsEnable();
    DriveLaneMotorReverse(feederLane);
    //Loop once for each 4mm of pitch
    while (feedingPitch--) {

        TMR1_Reload();

        //_____|____
        //____-|-___
        //Roll if still interrupted from previous session, exits when no photointerrupts 
        while (IsFrontSensorInterrupted(feederLane)) {
            //while (((IsRearSensorInterrupted(feederLane)) || (IsFrontSensorInterrupted(feederLane)))) {
            if (exceptionOccured) {
                break;
            }
        }

        //_--__|__________
        //_____|____--____
        //Roll while no photointerrupts, exits when photointerrupt occurs
        while (!IsFrontSensorInterrupted(feederLane)) {
            //while ((!IsRearSensorInterrupted(feederLane) && !IsFrontSensorInterrupted(feederLane))) {
            if (exceptionOccured) {
                break;
            }
        }

        //____--|__
        //_________
        //Roll until photointerrupt end, exits when no photointerrupts
        while (IsFrontSensorInterrupted(feederLane)) {
            //while ((IsRearSensorInterrupted(feederLane) || IsFrontSensorInterrupted(feederLane))) {
            if (exceptionOccured) {
                break;
            }

        }
    }

    BreakLaneMotor(feederLane);
    feedFinished = 1;

    DropGuard();

    __delay_ms(75);
    MotorsDisable();

}

void ExceptionGuard(uint32_t feederLane, uint32_t pitch) {

    TMR1_SetInterruptHandler(Exception);
    exceptionGuardLane = feederLane;
    feedFinished = 0;
    exceptionOccured = 0;
    feedTimeout = 0;
    TMR1_Reload();
    TMR1_StartTimer();
    //TMRs_Kick(exceptionTimer);
}

void DropGuard(void) {

    if (exceptionOccured) {
        EUSART1_Write('!'); //Feed failed
    } else {
        EUSART1_Write('%'); //Feed success
    }
}

void Exception(void) {
    if (!feedFinished) {
        exceptionOccured = 1;
    }
    feedTimeout = 1;

    TMR1_StopTimer();
}

/* *****************************************************************************
 End of File
 */
