// *****************************************************************************
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "sensors.h"

// *****************************************************************************

//defines
//structs
//varibles
//constants

// *****************************************************************************

//Local function definitions

// *****************************************************************************

void SensorsEnable(void) {
    SENSOR_EN_LAT = 1;
}

void SensorsDisable(void) {
    SENSOR_EN_LAT = 0;
}

bool IsFrontSensorInterrupted(uint8_t feederLane) {
    bool state;
    switch (feederLane) {
        case 1:
            state = (SENSOR_F_1_GetValue());
            break;
        case 2:
            state = (SENSOR_F_2_GetValue());
            break;
        case 3:
            state = (SENSOR_F_3_GetValue());
            break;
        case 4:
            state = (SENSOR_F_4_GetValue());

            break;
    }
    return (!state);
}

bool IsRearSensorInterrupted(uint8_t feederLane) {
    bool state;
    switch (feederLane) {
        case 1:
            state = (SENSOR_R_1_GetValue());
            break;
        case 2:
            state = (SENSOR_R_2_GetValue());
            break;
        case 3:
            state = (SENSOR_R_3_GetValue());
            break;
        case 4:
            state = (SENSOR_R_4_GetValue());

            break;
    }
    return (!state);
}


/* *****************************************************************************
 End of File
 */
