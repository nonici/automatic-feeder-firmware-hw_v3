/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef MOTORS_H    /* Guard against multiple inclusion */
#define MOTORS_H

// *****************************************************************************

#include "../mcc_generated_files/mcc.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

//Functions
void MotorsEnable(void);
void MotorsDisable(void);

void DriveLaneMotorForward(uint8_t feederLane);
void DriveLaneMotorReverse(uint8_t feederLane);
void BreakLaneMotor(uint8_t feederLane);
void HaltLaneMotor(uint8_t feederLane);
void DriveTapeMotorForward(void);
void DriveTapeMotorReverse(void);
void HaltTapeMotor(void);

// *****************************************************************************

#endif /* MOTORS_H */

/* *****************************************************************************
 End of File
 */
