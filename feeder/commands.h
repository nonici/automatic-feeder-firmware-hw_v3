/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef COMMANDS_H    /* Guard against multiple inclusion */
#define COMMANDS_H

// *****************************************************************************

#include <stdbool.h>
#include <stdint.h>

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

void FeedLane(uint8_t feederLane);
void AdvanceLane(uint8_t feederLane);
void RetractLane(uint8_t feederLane);

void StartTest (void);
void HaltTest (void);
bool IsTestingOn (void);

// *****************************************************************************

#endif /* COMMANDS_H */

/* *****************************************************************************
 End of File
 */
