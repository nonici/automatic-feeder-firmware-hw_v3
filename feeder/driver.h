/* ************************************************************************** */
/*
  @File Name
    filename.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef DRIVER_H    /* Guard against multiple inclusion */
#define DRIVER_H

// *****************************************************************************

#include "../mcc_generated_files/mcc.h"
#include "sensors.h"
#include "motors.h"

// *****************************************************************************

//#defines

// *****************************************************************************

//Variables

// *****************************************************************************

void AdvanceLaneForPitch(uint8_t feederLane, uint8_t feedingPitch);
void RetractLaneForPitch(uint8_t feederLane, uint8_t feedingPitch);

void Exception(void);

// *****************************************************************************

#endif /* DRIVER_H */

/* *****************************************************************************
 End of File
 */
