/* 
 * File: Timers.h / Timers.c  
 * Author: Flitch
 * Comments: Soft Timers up to 256 miliseconds using TMR_0 
 * Revision history: V1_I1
 */

#ifndef TIMERS_H
#define	TIMERS_H
#include "mcc_generated_files/mcc.h"

//Set required number of timers
#define     NUMBER_OF_TIMERS        10

void TMRs_Initialize(void);
//Initialization routine
//Uses TMR0
//Call after initialization of timer 0

void TMRs_Delayms(uint32_t mseconds);
//Creates delay in miliseconds

void TMRs_Tick(void);
//Ticks all soft timers
//Handles overflows

void TMRs_Kick(uint32_t tmr);
//Start or restart timer

uint32_t TMRs_Make(uint32_t msDelay, void* Handler);
//Create new timer
//Should be called at Initialization

void TMRs_End(uint32_t tmr);
//Stop selected timer

uint32_t TMRs_Get(uint32_t tmr);
//Get current time

//Makes timer permanent
void TMRs_MakePermanent(uint32_t tmr);
/*Permanent timers wont get killed on overflow, however it can still be killed manualy using TMRs_End*/


//Null pointer
inline void TMRs_NullPointer(void);

#endif	/* TIMERS_H */

