/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.6
        Device            :  PIC18F26Q10
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.30 and above
        MPLAB 	          :  MPLAB X 5.40	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set SENSOR_EN aliases
#define SENSOR_EN_TRIS                 TRISAbits.TRISA0
#define SENSOR_EN_LAT                  LATAbits.LATA0
#define SENSOR_EN_PORT                 PORTAbits.RA0
#define SENSOR_EN_WPU                  WPUAbits.WPUA0
#define SENSOR_EN_OD                   ODCONAbits.ODCA0
#define SENSOR_EN_ANS                  ANSELAbits.ANSELA0
#define SENSOR_EN_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define SENSOR_EN_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define SENSOR_EN_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define SENSOR_EN_GetValue()           PORTAbits.RA0
#define SENSOR_EN_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define SENSOR_EN_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define SENSOR_EN_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define SENSOR_EN_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define SENSOR_EN_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define SENSOR_EN_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define SENSOR_EN_SetAnalogMode()      do { ANSELAbits.ANSELA0 = 1; } while(0)
#define SENSOR_EN_SetDigitalMode()     do { ANSELAbits.ANSELA0 = 0; } while(0)

// get/set MOTOR_R_T aliases
#define MOTOR_R_T_TRIS                 TRISAbits.TRISA1
#define MOTOR_R_T_LAT                  LATAbits.LATA1
#define MOTOR_R_T_PORT                 PORTAbits.RA1
#define MOTOR_R_T_WPU                  WPUAbits.WPUA1
#define MOTOR_R_T_OD                   ODCONAbits.ODCA1
#define MOTOR_R_T_ANS                  ANSELAbits.ANSELA1
#define MOTOR_R_T_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define MOTOR_R_T_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define MOTOR_R_T_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define MOTOR_R_T_GetValue()           PORTAbits.RA1
#define MOTOR_R_T_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define MOTOR_R_T_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define MOTOR_R_T_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define MOTOR_R_T_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define MOTOR_R_T_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define MOTOR_R_T_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define MOTOR_R_T_SetAnalogMode()      do { ANSELAbits.ANSELA1 = 1; } while(0)
#define MOTOR_R_T_SetDigitalMode()     do { ANSELAbits.ANSELA1 = 0; } while(0)

// get/set MOTOR_F_T aliases
#define MOTOR_F_T_TRIS                 TRISAbits.TRISA2
#define MOTOR_F_T_LAT                  LATAbits.LATA2
#define MOTOR_F_T_PORT                 PORTAbits.RA2
#define MOTOR_F_T_WPU                  WPUAbits.WPUA2
#define MOTOR_F_T_OD                   ODCONAbits.ODCA2
#define MOTOR_F_T_ANS                  ANSELAbits.ANSELA2
#define MOTOR_F_T_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define MOTOR_F_T_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define MOTOR_F_T_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define MOTOR_F_T_GetValue()           PORTAbits.RA2
#define MOTOR_F_T_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define MOTOR_F_T_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define MOTOR_F_T_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define MOTOR_F_T_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define MOTOR_F_T_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define MOTOR_F_T_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define MOTOR_F_T_SetAnalogMode()      do { ANSELAbits.ANSELA2 = 1; } while(0)
#define MOTOR_F_T_SetDigitalMode()     do { ANSELAbits.ANSELA2 = 0; } while(0)

// get/set SENSOR_F_4 aliases
#define SENSOR_F_4_TRIS                 TRISAbits.TRISA3
#define SENSOR_F_4_LAT                  LATAbits.LATA3
#define SENSOR_F_4_PORT                 PORTAbits.RA3
#define SENSOR_F_4_WPU                  WPUAbits.WPUA3
#define SENSOR_F_4_OD                   ODCONAbits.ODCA3
#define SENSOR_F_4_ANS                  ANSELAbits.ANSELA3
#define SENSOR_F_4_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define SENSOR_F_4_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define SENSOR_F_4_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define SENSOR_F_4_GetValue()           PORTAbits.RA3
#define SENSOR_F_4_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define SENSOR_F_4_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define SENSOR_F_4_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define SENSOR_F_4_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define SENSOR_F_4_SetPushPull()        do { ODCONAbits.ODCA3 = 0; } while(0)
#define SENSOR_F_4_SetOpenDrain()       do { ODCONAbits.ODCA3 = 1; } while(0)
#define SENSOR_F_4_SetAnalogMode()      do { ANSELAbits.ANSELA3 = 1; } while(0)
#define SENSOR_F_4_SetDigitalMode()     do { ANSELAbits.ANSELA3 = 0; } while(0)

// get/set MOTOR_F_4 aliases
#define MOTOR_F_4_TRIS                 TRISAbits.TRISA4
#define MOTOR_F_4_LAT                  LATAbits.LATA4
#define MOTOR_F_4_PORT                 PORTAbits.RA4
#define MOTOR_F_4_WPU                  WPUAbits.WPUA4
#define MOTOR_F_4_OD                   ODCONAbits.ODCA4
#define MOTOR_F_4_ANS                  ANSELAbits.ANSELA4
#define MOTOR_F_4_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define MOTOR_F_4_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define MOTOR_F_4_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define MOTOR_F_4_GetValue()           PORTAbits.RA4
#define MOTOR_F_4_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define MOTOR_F_4_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define MOTOR_F_4_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define MOTOR_F_4_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define MOTOR_F_4_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define MOTOR_F_4_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define MOTOR_F_4_SetAnalogMode()      do { ANSELAbits.ANSELA4 = 1; } while(0)
#define MOTOR_F_4_SetDigitalMode()     do { ANSELAbits.ANSELA4 = 0; } while(0)

// get/set MOTOR_R_4 aliases
#define MOTOR_R_4_TRIS                 TRISAbits.TRISA5
#define MOTOR_R_4_LAT                  LATAbits.LATA5
#define MOTOR_R_4_PORT                 PORTAbits.RA5
#define MOTOR_R_4_WPU                  WPUAbits.WPUA5
#define MOTOR_R_4_OD                   ODCONAbits.ODCA5
#define MOTOR_R_4_ANS                  ANSELAbits.ANSELA5
#define MOTOR_R_4_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define MOTOR_R_4_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define MOTOR_R_4_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define MOTOR_R_4_GetValue()           PORTAbits.RA5
#define MOTOR_R_4_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define MOTOR_R_4_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define MOTOR_R_4_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define MOTOR_R_4_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define MOTOR_R_4_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define MOTOR_R_4_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define MOTOR_R_4_SetAnalogMode()      do { ANSELAbits.ANSELA5 = 1; } while(0)
#define MOTOR_R_4_SetDigitalMode()     do { ANSELAbits.ANSELA5 = 0; } while(0)

// get/set SENSOR_F_3 aliases
#define SENSOR_F_3_TRIS                 TRISAbits.TRISA6
#define SENSOR_F_3_LAT                  LATAbits.LATA6
#define SENSOR_F_3_PORT                 PORTAbits.RA6
#define SENSOR_F_3_WPU                  WPUAbits.WPUA6
#define SENSOR_F_3_OD                   ODCONAbits.ODCA6
#define SENSOR_F_3_ANS                  ANSELAbits.ANSELA6
#define SENSOR_F_3_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define SENSOR_F_3_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define SENSOR_F_3_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define SENSOR_F_3_GetValue()           PORTAbits.RA6
#define SENSOR_F_3_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define SENSOR_F_3_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define SENSOR_F_3_SetPullup()          do { WPUAbits.WPUA6 = 1; } while(0)
#define SENSOR_F_3_ResetPullup()        do { WPUAbits.WPUA6 = 0; } while(0)
#define SENSOR_F_3_SetPushPull()        do { ODCONAbits.ODCA6 = 0; } while(0)
#define SENSOR_F_3_SetOpenDrain()       do { ODCONAbits.ODCA6 = 1; } while(0)
#define SENSOR_F_3_SetAnalogMode()      do { ANSELAbits.ANSELA6 = 1; } while(0)
#define SENSOR_F_3_SetDigitalMode()     do { ANSELAbits.ANSELA6 = 0; } while(0)

// get/set SENSOR_R_4 aliases
#define SENSOR_R_4_TRIS                 TRISAbits.TRISA7
#define SENSOR_R_4_LAT                  LATAbits.LATA7
#define SENSOR_R_4_PORT                 PORTAbits.RA7
#define SENSOR_R_4_WPU                  WPUAbits.WPUA7
#define SENSOR_R_4_OD                   ODCONAbits.ODCA7
#define SENSOR_R_4_ANS                  ANSELAbits.ANSELA7
#define SENSOR_R_4_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define SENSOR_R_4_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define SENSOR_R_4_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define SENSOR_R_4_GetValue()           PORTAbits.RA7
#define SENSOR_R_4_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define SENSOR_R_4_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define SENSOR_R_4_SetPullup()          do { WPUAbits.WPUA7 = 1; } while(0)
#define SENSOR_R_4_ResetPullup()        do { WPUAbits.WPUA7 = 0; } while(0)
#define SENSOR_R_4_SetPushPull()        do { ODCONAbits.ODCA7 = 0; } while(0)
#define SENSOR_R_4_SetOpenDrain()       do { ODCONAbits.ODCA7 = 1; } while(0)
#define SENSOR_R_4_SetAnalogMode()      do { ANSELAbits.ANSELA7 = 1; } while(0)
#define SENSOR_R_4_SetDigitalMode()     do { ANSELAbits.ANSELA7 = 0; } while(0)

// get/set SENSOR_F_1 aliases
#define SENSOR_F_1_TRIS                 TRISBbits.TRISB0
#define SENSOR_F_1_LAT                  LATBbits.LATB0
#define SENSOR_F_1_PORT                 PORTBbits.RB0
#define SENSOR_F_1_WPU                  WPUBbits.WPUB0
#define SENSOR_F_1_OD                   ODCONBbits.ODCB0
#define SENSOR_F_1_ANS                  ANSELBbits.ANSELB0
#define SENSOR_F_1_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define SENSOR_F_1_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define SENSOR_F_1_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define SENSOR_F_1_GetValue()           PORTBbits.RB0
#define SENSOR_F_1_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define SENSOR_F_1_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define SENSOR_F_1_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define SENSOR_F_1_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define SENSOR_F_1_SetPushPull()        do { ODCONBbits.ODCB0 = 0; } while(0)
#define SENSOR_F_1_SetOpenDrain()       do { ODCONBbits.ODCB0 = 1; } while(0)
#define SENSOR_F_1_SetAnalogMode()      do { ANSELBbits.ANSELB0 = 1; } while(0)
#define SENSOR_F_1_SetDigitalMode()     do { ANSELBbits.ANSELB0 = 0; } while(0)

// get/set MOTOR_R_1 aliases
#define MOTOR_R_1_TRIS                 TRISBbits.TRISB1
#define MOTOR_R_1_LAT                  LATBbits.LATB1
#define MOTOR_R_1_PORT                 PORTBbits.RB1
#define MOTOR_R_1_WPU                  WPUBbits.WPUB1
#define MOTOR_R_1_OD                   ODCONBbits.ODCB1
#define MOTOR_R_1_ANS                  ANSELBbits.ANSELB1
#define MOTOR_R_1_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define MOTOR_R_1_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define MOTOR_R_1_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define MOTOR_R_1_GetValue()           PORTBbits.RB1
#define MOTOR_R_1_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define MOTOR_R_1_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define MOTOR_R_1_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define MOTOR_R_1_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define MOTOR_R_1_SetPushPull()        do { ODCONBbits.ODCB1 = 0; } while(0)
#define MOTOR_R_1_SetOpenDrain()       do { ODCONBbits.ODCB1 = 1; } while(0)
#define MOTOR_R_1_SetAnalogMode()      do { ANSELBbits.ANSELB1 = 1; } while(0)
#define MOTOR_R_1_SetDigitalMode()     do { ANSELBbits.ANSELB1 = 0; } while(0)

// get/set SENSOR_R_1 aliases
#define SENSOR_R_1_TRIS                 TRISBbits.TRISB2
#define SENSOR_R_1_LAT                  LATBbits.LATB2
#define SENSOR_R_1_PORT                 PORTBbits.RB2
#define SENSOR_R_1_WPU                  WPUBbits.WPUB2
#define SENSOR_R_1_OD                   ODCONBbits.ODCB2
#define SENSOR_R_1_ANS                  ANSELBbits.ANSELB2
#define SENSOR_R_1_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define SENSOR_R_1_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define SENSOR_R_1_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define SENSOR_R_1_GetValue()           PORTBbits.RB2
#define SENSOR_R_1_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define SENSOR_R_1_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define SENSOR_R_1_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define SENSOR_R_1_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define SENSOR_R_1_SetPushPull()        do { ODCONBbits.ODCB2 = 0; } while(0)
#define SENSOR_R_1_SetOpenDrain()       do { ODCONBbits.ODCB2 = 1; } while(0)
#define SENSOR_R_1_SetAnalogMode()      do { ANSELBbits.ANSELB2 = 1; } while(0)
#define SENSOR_R_1_SetDigitalMode()     do { ANSELBbits.ANSELB2 = 0; } while(0)

// get/set MOTOR_F_1 aliases
#define MOTOR_F_1_TRIS                 TRISBbits.TRISB3
#define MOTOR_F_1_LAT                  LATBbits.LATB3
#define MOTOR_F_1_PORT                 PORTBbits.RB3
#define MOTOR_F_1_WPU                  WPUBbits.WPUB3
#define MOTOR_F_1_OD                   ODCONBbits.ODCB3
#define MOTOR_F_1_ANS                  ANSELBbits.ANSELB3
#define MOTOR_F_1_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define MOTOR_F_1_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define MOTOR_F_1_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define MOTOR_F_1_GetValue()           PORTBbits.RB3
#define MOTOR_F_1_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define MOTOR_F_1_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define MOTOR_F_1_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define MOTOR_F_1_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define MOTOR_F_1_SetPushPull()        do { ODCONBbits.ODCB3 = 0; } while(0)
#define MOTOR_F_1_SetOpenDrain()       do { ODCONBbits.ODCB3 = 1; } while(0)
#define MOTOR_F_1_SetAnalogMode()      do { ANSELBbits.ANSELB3 = 1; } while(0)
#define MOTOR_F_1_SetDigitalMode()     do { ANSELBbits.ANSELB3 = 0; } while(0)

// get/set RB4 procedures
#define RB4_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define RB4_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define RB4_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define RB4_GetValue()              PORTBbits.RB4
#define RB4_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define RB4_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define RB4_SetPullup()             do { WPUBbits.WPUB4 = 1; } while(0)
#define RB4_ResetPullup()           do { WPUBbits.WPUB4 = 0; } while(0)
#define RB4_SetAnalogMode()         do { ANSELBbits.ANSELB4 = 1; } while(0)
#define RB4_SetDigitalMode()        do { ANSELBbits.ANSELB4 = 0; } while(0)

// get/set RB5 procedures
#define RB5_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define RB5_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define RB5_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define RB5_GetValue()              PORTBbits.RB5
#define RB5_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define RB5_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define RB5_SetPullup()             do { WPUBbits.WPUB5 = 1; } while(0)
#define RB5_ResetPullup()           do { WPUBbits.WPUB5 = 0; } while(0)
#define RB5_SetAnalogMode()         do { ANSELBbits.ANSELB5 = 1; } while(0)
#define RB5_SetDigitalMode()        do { ANSELBbits.ANSELB5 = 0; } while(0)

// get/set MOTOR_F_3 aliases
#define MOTOR_F_3_TRIS                 TRISCbits.TRISC0
#define MOTOR_F_3_LAT                  LATCbits.LATC0
#define MOTOR_F_3_PORT                 PORTCbits.RC0
#define MOTOR_F_3_WPU                  WPUCbits.WPUC0
#define MOTOR_F_3_OD                   ODCONCbits.ODCC0
#define MOTOR_F_3_ANS                  ANSELCbits.ANSELC0
#define MOTOR_F_3_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define MOTOR_F_3_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define MOTOR_F_3_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define MOTOR_F_3_GetValue()           PORTCbits.RC0
#define MOTOR_F_3_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define MOTOR_F_3_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define MOTOR_F_3_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define MOTOR_F_3_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define MOTOR_F_3_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define MOTOR_F_3_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define MOTOR_F_3_SetAnalogMode()      do { ANSELCbits.ANSELC0 = 1; } while(0)
#define MOTOR_F_3_SetDigitalMode()     do { ANSELCbits.ANSELC0 = 0; } while(0)

// get/set MOTOR_R_3 aliases
#define MOTOR_R_3_TRIS                 TRISCbits.TRISC1
#define MOTOR_R_3_LAT                  LATCbits.LATC1
#define MOTOR_R_3_PORT                 PORTCbits.RC1
#define MOTOR_R_3_WPU                  WPUCbits.WPUC1
#define MOTOR_R_3_OD                   ODCONCbits.ODCC1
#define MOTOR_R_3_ANS                  ANSELCbits.ANSELC1
#define MOTOR_R_3_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define MOTOR_R_3_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define MOTOR_R_3_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define MOTOR_R_3_GetValue()           PORTCbits.RC1
#define MOTOR_R_3_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define MOTOR_R_3_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define MOTOR_R_3_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define MOTOR_R_3_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define MOTOR_R_3_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define MOTOR_R_3_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define MOTOR_R_3_SetAnalogMode()      do { ANSELCbits.ANSELC1 = 1; } while(0)
#define MOTOR_R_3_SetDigitalMode()     do { ANSELCbits.ANSELC1 = 0; } while(0)

// get/set MOTOR_SLP aliases
#define MOTOR_SLP_TRIS                 TRISCbits.TRISC2
#define MOTOR_SLP_LAT                  LATCbits.LATC2
#define MOTOR_SLP_PORT                 PORTCbits.RC2
#define MOTOR_SLP_WPU                  WPUCbits.WPUC2
#define MOTOR_SLP_OD                   ODCONCbits.ODCC2
#define MOTOR_SLP_ANS                  ANSELCbits.ANSELC2
#define MOTOR_SLP_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define MOTOR_SLP_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define MOTOR_SLP_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define MOTOR_SLP_GetValue()           PORTCbits.RC2
#define MOTOR_SLP_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define MOTOR_SLP_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define MOTOR_SLP_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define MOTOR_SLP_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define MOTOR_SLP_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define MOTOR_SLP_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define MOTOR_SLP_SetAnalogMode()      do { ANSELCbits.ANSELC2 = 1; } while(0)
#define MOTOR_SLP_SetDigitalMode()     do { ANSELCbits.ANSELC2 = 0; } while(0)

// get/set SENSOR_R_3 aliases
#define SENSOR_R_3_TRIS                 TRISCbits.TRISC3
#define SENSOR_R_3_LAT                  LATCbits.LATC3
#define SENSOR_R_3_PORT                 PORTCbits.RC3
#define SENSOR_R_3_WPU                  WPUCbits.WPUC3
#define SENSOR_R_3_OD                   ODCONCbits.ODCC3
#define SENSOR_R_3_ANS                  ANSELCbits.ANSELC3
#define SENSOR_R_3_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define SENSOR_R_3_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define SENSOR_R_3_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define SENSOR_R_3_GetValue()           PORTCbits.RC3
#define SENSOR_R_3_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define SENSOR_R_3_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define SENSOR_R_3_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define SENSOR_R_3_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define SENSOR_R_3_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define SENSOR_R_3_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define SENSOR_R_3_SetAnalogMode()      do { ANSELCbits.ANSELC3 = 1; } while(0)
#define SENSOR_R_3_SetDigitalMode()     do { ANSELCbits.ANSELC3 = 0; } while(0)

// get/set SENSOR_F_2 aliases
#define SENSOR_F_2_TRIS                 TRISCbits.TRISC4
#define SENSOR_F_2_LAT                  LATCbits.LATC4
#define SENSOR_F_2_PORT                 PORTCbits.RC4
#define SENSOR_F_2_WPU                  WPUCbits.WPUC4
#define SENSOR_F_2_OD                   ODCONCbits.ODCC4
#define SENSOR_F_2_ANS                  ANSELCbits.ANSELC4
#define SENSOR_F_2_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define SENSOR_F_2_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define SENSOR_F_2_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define SENSOR_F_2_GetValue()           PORTCbits.RC4
#define SENSOR_F_2_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define SENSOR_F_2_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define SENSOR_F_2_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define SENSOR_F_2_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define SENSOR_F_2_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define SENSOR_F_2_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define SENSOR_F_2_SetAnalogMode()      do { ANSELCbits.ANSELC4 = 1; } while(0)
#define SENSOR_F_2_SetDigitalMode()     do { ANSELCbits.ANSELC4 = 0; } while(0)

// get/set SENSOR_R_2 aliases
#define SENSOR_R_2_TRIS                 TRISCbits.TRISC5
#define SENSOR_R_2_LAT                  LATCbits.LATC5
#define SENSOR_R_2_PORT                 PORTCbits.RC5
#define SENSOR_R_2_WPU                  WPUCbits.WPUC5
#define SENSOR_R_2_OD                   ODCONCbits.ODCC5
#define SENSOR_R_2_ANS                  ANSELCbits.ANSELC5
#define SENSOR_R_2_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define SENSOR_R_2_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define SENSOR_R_2_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define SENSOR_R_2_GetValue()           PORTCbits.RC5
#define SENSOR_R_2_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define SENSOR_R_2_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define SENSOR_R_2_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define SENSOR_R_2_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define SENSOR_R_2_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define SENSOR_R_2_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define SENSOR_R_2_SetAnalogMode()      do { ANSELCbits.ANSELC5 = 1; } while(0)
#define SENSOR_R_2_SetDigitalMode()     do { ANSELCbits.ANSELC5 = 0; } while(0)

// get/set MOTOR_F_2 aliases
#define MOTOR_F_2_TRIS                 TRISCbits.TRISC6
#define MOTOR_F_2_LAT                  LATCbits.LATC6
#define MOTOR_F_2_PORT                 PORTCbits.RC6
#define MOTOR_F_2_WPU                  WPUCbits.WPUC6
#define MOTOR_F_2_OD                   ODCONCbits.ODCC6
#define MOTOR_F_2_ANS                  ANSELCbits.ANSELC6
#define MOTOR_F_2_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define MOTOR_F_2_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define MOTOR_F_2_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define MOTOR_F_2_GetValue()           PORTCbits.RC6
#define MOTOR_F_2_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define MOTOR_F_2_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define MOTOR_F_2_SetPullup()          do { WPUCbits.WPUC6 = 1; } while(0)
#define MOTOR_F_2_ResetPullup()        do { WPUCbits.WPUC6 = 0; } while(0)
#define MOTOR_F_2_SetPushPull()        do { ODCONCbits.ODCC6 = 0; } while(0)
#define MOTOR_F_2_SetOpenDrain()       do { ODCONCbits.ODCC6 = 1; } while(0)
#define MOTOR_F_2_SetAnalogMode()      do { ANSELCbits.ANSELC6 = 1; } while(0)
#define MOTOR_F_2_SetDigitalMode()     do { ANSELCbits.ANSELC6 = 0; } while(0)

// get/set MOTOR_R_2 aliases
#define MOTOR_R_2_TRIS                 TRISCbits.TRISC7
#define MOTOR_R_2_LAT                  LATCbits.LATC7
#define MOTOR_R_2_PORT                 PORTCbits.RC7
#define MOTOR_R_2_WPU                  WPUCbits.WPUC7
#define MOTOR_R_2_OD                   ODCONCbits.ODCC7
#define MOTOR_R_2_ANS                  ANSELCbits.ANSELC7
#define MOTOR_R_2_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define MOTOR_R_2_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define MOTOR_R_2_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define MOTOR_R_2_GetValue()           PORTCbits.RC7
#define MOTOR_R_2_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define MOTOR_R_2_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define MOTOR_R_2_SetPullup()          do { WPUCbits.WPUC7 = 1; } while(0)
#define MOTOR_R_2_ResetPullup()        do { WPUCbits.WPUC7 = 0; } while(0)
#define MOTOR_R_2_SetPushPull()        do { ODCONCbits.ODCC7 = 0; } while(0)
#define MOTOR_R_2_SetOpenDrain()       do { ODCONCbits.ODCC7 = 1; } while(0)
#define MOTOR_R_2_SetAnalogMode()      do { ANSELCbits.ANSELC7 = 1; } while(0)
#define MOTOR_R_2_SetDigitalMode()     do { ANSELCbits.ANSELC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/