/*
 * File:   Timers.c
 * Author: Flitch
 * Universal static timer routines
 * Created on May 9, 2017, 8:57 PM
 */


#include "Timers.h"

typedef struct {
    void (*TMR_Handler)(void);
    bool append;
    bool active;
    bool busy;
    bool permanent;
    uint32_t loadTime;
    uint32_t elapsedTime;
} TMRS;

TMRS tmrS[NUMBER_OF_TIMERS] = {0};

uint32_t tmrCounter;

void TMRs_Initialize(void) {
    //Allocate hardware TMR0 to Timers
    TMR1_SetInterruptHandler(&TMRs_Tick);

    //Initialize fn pointers to NULL
    uint32_t i;
    for (i = 0; i == NUMBER_OF_TIMERS; i++) {

        tmrS[i].TMR_Handler = TMRs_NullPointer;
    }

    //Clear timer counter
    tmrCounter = 0;

    //Begin hardware timer 1
    TMR1_Start();
}

void TMRs_Delayms(uint32_t mseconds) {

    //Read current counts
    uint32_t delayStartTime = tmrCounter;

    while ((tmrCounter - delayStartTime) < (mseconds));

}

void TMRs_Tick(void) {

    //Increment timer counter

    tmrCounter++;
    tmrCounter %= 0xFFFFFFFF;

    //Tick all active timers
    uint32_t tmr;
    for (tmr = 0; tmr < NUMBER_OF_TIMERS; tmr++) {
        if ((tmrS[tmr].active) && (tmrS[tmr].elapsedTime)) {
            tmrS[tmr].elapsedTime--;
        }
    }

    //Handle timer overruns
    for (tmr = 0; tmr < NUMBER_OF_TIMERS; tmr++) {
        if ((tmrS[tmr].active) && (!tmrS[tmr].elapsedTime)) {
            tmrS[tmr].TMR_Handler();
            tmrS[tmr].active = 0;
            if (!(tmrS[tmr].permanent)) {
                TMRs_End(tmr);
            }
        }
    }

    //Activate appended timers
    for (tmr = 0; tmr < NUMBER_OF_TIMERS; tmr++) {
        if (tmrS[tmr].append) {
            tmrS[tmr].elapsedTime = tmrS[tmr].loadTime;
            tmrS[tmr].append = 0;
            tmrS[tmr].active = 1;
        }
    }
}

void TMRs_Kick(uint32_t tmr) {
    tmrS[tmr].append = 1;
}

uint32_t TMRs_Make(uint32_t msDelay, void* Handler) {

    uint32_t tmr = 0;

    //Find first free timer and reserve it
    for (tmr; tmr < NUMBER_OF_TIMERS; tmr++) {

        if (!tmrS[tmr].busy) {
            tmrS[tmr].busy = 1;
            break;
        }
    }

    //Initialize parameters
    tmrS[tmr].TMR_Handler = Handler;
    tmrS[tmr].loadTime = msDelay;

    //Report timer index
    return tmr;
}

void TMRs_End(uint32_t tmr) {
    tmrS[tmr].TMR_Handler = TMRs_NullPointer;
    tmrS[tmr].active = 0;
    tmrS[tmr].busy = 0;
}

void TMRs_MakePermanent(uint32_t tmr) {
    tmrS[tmr].permanent = 1;
}

uint32_t TMRs_Get(uint32_t tmr) {
    return (tmrS[tmr].elapsedTime);
}

inline void TMRs_NullPointer(void) {
    ;
}
